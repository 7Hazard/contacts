#pragma once

/**
 * Main header for contacts app
 * Includes all headers needed for source files
 * */

#include <string>
#include <iostream>

#include "utils/string.hpp"
#include "utils/io.hpp"
#include "utils/json.hpp"

#include "Command.hpp"

namespace Contacts {

// Load contacts into the passed contacts json object
static bool Load(json& contacts)
{
    if(!Utils::ReadFromFile("contacts.json", contacts))
    {
        std::cerr << "Could not open contacts.json, might be in use or is corrupted." << std::endl;
        std::cout << "\tYou can use 'wipe' command to make a clean contactsbook." << std::endl;
        
        return false;
    }

    return true;
}

static bool Save(json& contacts)
{
    if(!Utils::WriteToFile("contacts.json", contacts))
    {
        std::cerr << "Could not open contacts.json, might be in use." << std::endl;
        return false;
    }

    return true;
}

static bool Exists(json& contacts, std::string name)
{
    // Check if contact already exists
    try
    {
        contacts.at(name);
    }
    catch(const std::exception&)
    {
        return false;
    }

    return true;
}

}