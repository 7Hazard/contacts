#pragma once

#include <string>
#include <algorithm>
#include <vector>
#include <tuple>

#include "Command.hpp"

namespace Utils {

// Changes all characters in a string to lowercase and returns the result
static std::string ToLower(std::string str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    return str;
}

static bool Contains(const std::string& str, const std::string& find)
{
    return str.find(find) != std::string::npos;
}

// Splits a string by it's delimiter
static std::vector<std::string> Split(std::string str, std::string delimiter)
{
    // https://stackoverflow.com/a/14266139

    std::vector<std::string> elements;
    size_t pos = 0;
    std::string token;
    while ((pos = str.find(delimiter)) != std::string::npos) {
        elements.push_back(str.substr(0, pos));
        str.erase(0, pos + delimiter.length());
    }

    return elements;
}

/*
static std::tuple<bool, std::string, std::vector<std::string>> ParseCommandline(std::string line)
{
    auto delems = Split(line, " ");

    std::string cmd = delems[0];
    std::vector<std::string>> elems;

    for(size_t i = 1, i < delems.size(), i++)
    {
        // Checking for "
        size_t pos = delem.find('"');
        if(pos == 0)
        {
            bool endFound = false;
            for(auto lastDelim : delems)
            {
                if(lastDelim.find('"') > 0){
                    endFound = true;
                    break;
                }
                
                arg+=lastDelim;
            }

            if(!endFound) // Malformed
            {
                return std::make_tuple(false, cmd, elems);
            }
            else
            {
                elems.push_back()
            }
        }
        else if(pos > 0) // Malformed
        {
            return std::make_tuple(false, cmd, elems);
        }

        pos = delem.find('\'');
        if(pos == 0)
        {
            bool endFound = false;
            for(auto lastDelim : delems)
            {
                if(lastDelim.find('\'') > 0){
                    endFound = true;
                    break;
                }
                
                arg+=lastDelim;
            }

            if(!endFound) // Malformed
            {
                return std::make_tuple(false, cmd, elems);
            }
            else
            {
                elems.push_back()
            }
        }
        else if(pos > 0) // Malformed
        {
            return std::make_tuple(false, cmd, elems);
        }
    }
}
*/

}