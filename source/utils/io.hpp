#pragma once

#include <iostream>
#include <string>

namespace Utils {

// Helper funtion to safely get an input from the user and return it as a string
static std::string GetInput(std::string inputMsg = "", bool newLine = false)
{
    std::cout << inputMsg << ": ";
    if(newLine) std::cout << std::endl;
    std::string in;
    std::getline(std::cin, in);
    return in;
}

}