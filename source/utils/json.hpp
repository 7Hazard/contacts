#pragma once

#include <fstream>
#include <iomanip>

#include "json/json.hpp"
using json = nlohmann::json;

namespace Utils {

/**
 * Reads and parses the json from a file
 * If the file doesn't exist, it creates it.
 * If the file cannot be create or be read from, return false.
 * Otherwise, it returns true, meaning it succeeded.
 * */
static bool ReadFromFile(std::string filepath, nlohmann::json& data)
{
    std::ifstream in(filepath);
    std::ofstream out;

    // File failed to open, maybe doesn't exist?
    if(in.fail())
    {
        // Then try to create the file
        in.close();
        out.open(filepath);

        // Still no luck?
        if(out.fail())
        {
            // Give up
            out.close();

            // Well the OS is grumpy, we can't work with this file right now
            return false;
        }
        
        // If it creates it, write an empty json array
        out << json::array();

        // We done here
        out.close();
    }

    in.open(filepath);
    
    // The json file might be corrupted
    try
    {
        in >> data;
    }
    catch(const std::exception&)
    {
        // Probably corrupted json
        return false;
    }
    
    return true;
}

static bool WriteToFile(std::string filepath, json& data, bool pretty = true)
{
    std::ofstream out(filepath);

    // File failed to open, maybe doesn't exist?
    if(out.fail())
    {
        // Give up
        out.close();

        // Well the OS is grumpy, we can't work with this file right now
        return false;
    }

    if(pretty) out << std::setw(4); // Will indent nicely
    out << data; // Write json to file
    out.close();

    return true;
}

}
