#pragma once

#include <functional>
#include <vector>
#include <map>

class Command
{
public: // Public members that are accessible from outside of the class
    /**
     * Command::Args represents a class
     * that contains arguments which can be used by commands.
     * */
    class Args
    {
    private:
        std::vector<std::string> args;

    public:
        Args() {};
        Args(const std::vector<std::string>& vec) : args(vec) {};

        // Push a new argument
        void Push(std::string arg);

        // Gets an argument by it's index
        std::string Get(size_t index);
        inline std::string operator[](size_t index) { return Get(index); };

        // Returns the count of pushed arguments
        size_t Count();

        // Funtions to iterate over the args, like with a ranged for loop
        inline std::vector<std::string>::iterator begin() { return args.begin(); }
        inline std::vector<std::string>::iterator end() { return args.end(); }
    };

    using Action = std::function<int(Args args)>;

private: // Class members that is only accessible from inside of the class
    Action action;

    /**
     * A Key-Value map which contains all instances to commands
     * where the key is a string name of the command
     * and the value is a pointer to the instance of the command
     * */
    static std::map<std::string, Command*> commands;

public: // Members that is accessible from outside of the class
    const std::string description;

    // The constructor for a command
    Command(std::string name, std::string description, Command::Action action);

    // Executes a function directly with pre-processed arguments
    void Execute(Command::Args args = Command::Args());

    // Gets a constant reference to the commands map (eg. for iteration)
    static inline const std::map<std::string, Command*>& GetMap() { return commands; };

    // Gets a command by it's name
    static Command* Get(std::string name);
};
