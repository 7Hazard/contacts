#include "Command.hpp"

#include <iostream>

std::map<std::string, Command*> Command::commands;

// Command impls
Command::Command(std::string name, std::string description, Command::Action action)
    : description(description), action(action) // Initialize members
{
    // Add the command to the commands map
    commands.emplace(name, this);
}

Command* Command::Get(std::string name)
{
    try
    {
        return commands.at(name);
    }
    catch(const std::exception& e)
    {
        return nullptr;
    }
}

void Command::Execute(Command::Args args)
{
    int result = action(args);
    if(result != 0)
    {
        std::cout << "The command finished with code " << result << std::endl;
    }
    std::cout << std::endl;
}

// Command::Args impls
void Command::Args::Push(std::string arg)
{
    args.push_back(arg);
}

std::string Command::Args::Get(size_t index)
{
    return args[index];
}

size_t Command::Args::Count()
{
    return args.size();
}