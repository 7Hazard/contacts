#include "Contacts.hpp"

#include <iostream>
#include <string>

int func(Command::Args args)
{
    std::cout << "The following commands are avaialable:\n" << std::endl;

    for(auto command : Command::GetMap())
    {
        // command.first: name of command
        // command.second: pointer to the command instance
        std::cout << command.first << ": " << command.second->description << std::endl;
    }
    std::cout << std::endl << "Some commands support '<command> help' outside the Command Line Interface to display more detailed command specific help." << std::endl
        << "\teg. 'add help'" << std::endl;

    return 0;
}

Command cmd("help", "Shows this help dialog", func);
