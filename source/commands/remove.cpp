#include "Contacts.hpp"
using namespace Contacts;

#include <iostream>

// Then make the command available
Command removecmd("remove", "Removes a contact from the contactbook", [](Command::Args args)->int {
    json contacts;

    // Read from the contacts data file, write it into the contacts json data object
    if(!Contacts::Load(contacts)) return -1;
    
    std::cout << "Enter the full name you wish to remove from the contactbook" << std::endl;
    std::string name = Utils::GetInput("Name");

    if(!Contacts::Exists(contacts, name))
    {
        std::cerr << "The contact does not exist!" << std::endl;
        return -1;
    }

    // Ensure that he intends to delete this contact
    std::cout << "Are you sure you want to remove '" << name << "'" << std::endl; 
    auto answer = Utils::GetInput("[yes/no]");

    contacts.erase(name);
    Contacts::Save(contacts);
    std::cout << "'" << name << "' has been removed from your contactbook" << std::endl;
    
    return 0;
});
