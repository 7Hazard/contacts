#include "Contacts.hpp"
using namespace Contacts;

#include <set>

Command findcmd("find", "Performs a search for users by specified keyword and filters", [](Command::Args args)->int {
    json contacts;

    // Read from the contacts data file, write it into the contacts json data object
    if(!Contacts::Load(contacts)) return -1;

    std::set<std::string> filters;
    std::string keyword;

    // Handle for usage from shell command
    if(args.Count() > 0)
    {
        if(args[0] == "help")
        {
            std::cout << "Format: find <keyword> <filter1> <filter2>..." << std::endl;
            return 0;
        }

        keyword = args[0];
        for(size_t i = 1; i < args.Count(); i++)
        {
            filters.insert(args[i]);
        }
    }
    else // Usage from the CLI
    {
        std::cout << "Which contact fields do you want to filter?" << std::endl 
            << "\t eg. name, number, mail, address or even a custom one" << std::endl
            << "\t enter 'stop' when you have added your filters" << std::endl;

        while(true)
        {
            std::string filter = Utils::GetInput("Filter");
            if(filter=="stop") break;
            filters.insert(filter);
        }

        if(filters.size() < 1)
        {
            std::cout << "No filters were added for a search" << std::endl;
            return -1;
        }

        std::cout << std::endl << "Enter the keyword you are searching for" << std::endl;
        keyword = Utils::GetInput("Keyword");
    }
    
    std::cout << "\nSearching..." << std::endl;
    json results = json::object();
    for(auto& [name, fields] : contacts.items())
    {
        json matches; // will look like -> name: { "email": ... }
        for(auto& filter : filters)
        {
            try
            {
                std::string field = fields.at(filter); // Will except here if contact doesn't have the filtered field
                if(field.find(keyword) != std::string::npos)
                {
                    matches[filter] = field;
                }
            }
            catch(const std::exception&)
            {
                continue; // Just continue with next filter if the contact doesn't have the field
            }
        }

        // If there were matches for this contact, put it in the results
        if(!matches.empty())
        {
            results[name] = matches;
        }
    }

    // Write to file
    std::string filename = "find "+keyword+".json";
    Utils::WriteToFile(filename, results);
    std::cout << "Output results to '" << filename << "'" << std::endl;

    // Ask if user wants results output to console
    std::cout << "Ouput results to console?" << std::endl;
    auto answer = Utils::GetInput("[yes/no]");
    if(answer == "yes" || answer == "y")
    {
        std::cout << std::endl;
        for(auto& [name, fields] : results.items())
        {
            std::cout << name << std::endl;
            
            for(auto& [field, value] : fields.items())
            {
                std::cout << "|- " << field << ": " << value << std::endl;
            }
        }
    }

    // Summary
    std::cout << std::endl << "Fetched " << results.size()
        << " out of " << contacts.size()
        << " when searching for '" << keyword << "'"
        << " in fields ";
    for(auto& filter : filters)
    {
        size_t i = 1;
        
        std::cout << filter << (i!=filters.size() ? ", " : "");

        i++;
    }
    std::cout << std::endl;

    return 0;
});
