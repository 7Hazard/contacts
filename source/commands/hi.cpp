// Simple hi command, primarily for testing

#include "Contacts.hpp"

#include <iostream>
#include <string>

/**
 * Example and test command
 * This can be used to create other commands
 * The constructor takes in 
 * - a string name,
 * - a function with parameters (Command::Args) and return type 'int'
 * The function will be called when executing the command
 * and returns 0 if the command was successful.
 * */

// Create the function that will be called when executing the command
int hifunc(Command::Args args)
{
    std::cout << "Hello ";

    for(auto arg : args)
    {
        std::cout << arg << " ";
    }
    std::cout << std::endl;
    
    return 0;
}

// Then make the command available
Command hicmd("hi", "Just replies 'Hello' with any args that are passed", hifunc);
