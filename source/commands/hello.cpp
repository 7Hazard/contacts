#include "Contacts.hpp"

/**
 * The following command passes lambda functions
 * instead of explicitly made functions.
 * You can also declare the lambdas into variables and pass them instead.
 * The principal is the same.
 * */
Command hellocmd("hello", "Just replies 'Hi' with any passed args", [](Command::Args args)->int {
        std::cout << "Hi ";

        for(auto arg : args)
        {
            std::cout << arg << " ";
        }
        std::cout << std::endl;
        
        // Returns a non-zero result to mark that the command possibly didn't execute successfully
        // Just for demonstration purposes 
        return -1;
    }
);
