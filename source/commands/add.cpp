#include "Contacts.hpp"

#include <iostream>
#include <string>

using Utils::GetInput;

Command addcmd("add", "Adds a new contact", [](Command::Args args)->int {
    json contacts;

    // Requests the user to overwrite if the contact already exists
    auto RequestOverwrite = [&](std::string name)->bool
    {
        // Check if contact already exists
        if(Contacts::Exists(contacts, name))
        {
            std::cout << "This contact already exists! Are you sure you want to overwrite this contact?" << std::endl;
            std::string answer = GetInput("[yes/no]");
            if(answer == "no" || answer == "n")
            {
                std::cout << "Contact add canceled" << std::endl;
                return false;
            }
        }

        return true;
    };

    // Adds a new contact
    auto AddContact = [&](std::string name, json newContact)->int
    {
        // Add/overwrite the new contact to the contactbook
        contacts[name] = newContact;

        // Save the updated contactsbook to the file
        // If the changed cannot be written to the file, keep asking for retry
        while(!Contacts::Save(contacts))
        {
            std::cout << "Do you want to try again?" << std::endl;
            std::string answer = GetInput("[yes|no]");
            answer = Utils::ToLower(answer);
            if(answer == "no" || answer == "n")
            {
                // User doesn't want to try again, cancel add
                return -1;
            }
            else
            {
                std::cout << "Retrying..." << std::endl;
            }
        }

        std::cout << "Added new contact" << std::endl;

        return 0;
    };

    // Read from the contacts data file, write it into the contacts json data object
    if(!Contacts::Load(contacts)) return -1;
    if(!contacts.is_object()) contacts = json::object(); // Might not even be a json object at all

    // If arguemnts were passed directly, validate and create a new contact by that
    if(args.Count() > 0)
    {
        // Display help if 'help' was passed as the first argument
        if(args[0] == "help")
        {
            std::cout << "add: Adds a new contact to your contactbook" << std::endl
                << "If you want add a new user instantly you can do 'add 'name:John Doe' 'number:+46-78 996' 'custom field:anything' ..." << std::endl;
            return 0;
        }

        std::string name;
        json newContact;

        // Check each arguemnt, validate and add to contact details
        for(auto arg : args)
        {
            // Get the index for the ':'
            size_t separatorPos = arg.find(':');
            if(separatorPos < 0) // Separator not found, unknown argument was passed
            {
                std::cout << "Unknown arguemnt passed '" << arg << "', ignored..." << std::endl;
                continue; // Parse the rest of the arguments
            }

            std::string fieldName = arg.substr(0, separatorPos);
            std::string fieldValue = arg.substr(separatorPos+1, arg.length());

            // If its the name, put it in the name vairable instead of in the json
            if(fieldName == "name") 
            {
                name = fieldValue;
                if(!RequestOverwrite(name)) return -1;
                newContact[name] = name;
            }
            else newContact[fieldName] = fieldValue;
        }

        // If string is still empty, the new user cannot be added, cancel add
        if(name.empty())
        {
            std::cerr << "'name:' argument is required to add a new contact" << std::endl;
            return -1;
        }

        // Else go ahead and add the new contact
        return AddContact(name, newContact);
    }
    
    // If no arguments were passed, do it via an interface
    
    // Ask for the name
    std::string name = GetInput("Name");
    if(!RequestOverwrite(name)) return -1;

    // Ask for the rest of the contact details
    json newContact = {
        {"name", name},
        {"number", GetInput("Number")},
        {"email", GetInput("Email")},
        {"address", GetInput("Address")}
    };

    // Ask the user if he wishes to put in any custom contact fields
    while(true){
        std::cout << "Do you want to add another custom field?" << std::endl;
        std::string answer = GetInput("[yes/no]");
        answer = Utils::ToLower(answer);
        if(answer == "yes" || answer == "y")
        {
            std::string fieldName = GetInput("Field name");
            std::string fieldValue = GetInput(fieldName);
            newContact[fieldName] = fieldValue;
        }
        else break; // User doesn't want to add any new fields
    }

    // Will return the result code
    return AddContact(name, newContact);
});
