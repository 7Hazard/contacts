#include "Contacts.hpp"

#include <iostream>
#include <string>

Command wipecmd("wipe", "Will wipe clean the whole contactbook", [](Command::Args args)->int {
    // Ask for certainty
    std::cout << "Are you sure you want to wipe?" << std::endl;
    std::cout << "WARNING: THIS WILL REMOVE ALL OF YOUR PREVIOUS CONTACTS" << std::endl;

    {
        std::string in = Utils::GetInput("[yes/no]");
        Utils::ToLower(in);
        if(in != "y" && in != "yes")
        {
            std::cout << "The corrupted contactbook will remain" << std::endl;
            return 0;
        }
    }

    // Ask again, this ain't a game
    {
        std::cout << "ARE YOU COMPLETELY SURE???" << std::endl;
        std::string in = Utils::GetInput("[yes/no]");
        in = Utils::ToLower(in);
        if(in == "y" || in == "yes")
        {
            std::cout << "No going back now..." << std::endl;
            json contacts = json::object();
            if(!Utils::WriteToFile("contacts.json", contacts))
            {
                std::cerr << "Could not write to contacts.json" << std::endl;
                return -1;
            }
            std::cout << "Wrote a new, clean and empty contactbook" << std::endl;
        }
        else
        {
            std::cout << "The corrupted contactbook will remain" << std::endl;
            return 0;
        }
    }

    return 0;
});
