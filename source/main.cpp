#include <iostream>
#include <string>

#include "Contacts.hpp"

// Command executor helper function
int ExecuteCommand(std::string name, Command::Args args = Command::Args())
{
    // Get the command
    Command* command = Command::Get(name);

    // If the command is null, the command doesnt exist
    if(command == nullptr)
    {
        std::cerr << "Unknown command '" << name << "'" << std::endl;
        return -1;
    }

    // Execute the command
    command->Execute(args);

    return 0;
}

/**
 * The entry point for the app
 * */
int main(int argc, char const *argv[])
{
    // Program started without a passed command, start command line interface
    if (argc < 2)
    {
        std::cout <<
            "############################################\n"
            "###  Contacts - Contactbook utility CLI  ###\n"
            "############################################\n"
            << std::endl
            << "Type 'help' to see the available commands" << std::endl
            << "Type 'exit' or 'quit' to close the app" << std::endl << std::endl;

        std::string in;
        while(true)
        {
            in = Utils::GetInput();

            if(in=="exit" || in=="quit")
            {
                return 0;
            }
            else if(in.find(' ') != std::string::npos)
            {
                /*auto cmdl = Utils::ParseCommandline(in);
                if(!std::get<0>(cmdl)) // If command was malformed
                {
                    std::cout << "Input was malformed" << std::endl << std::endl;
                    continue;
                }

                auto command = std::get<1>(cmdl);
                Command::Args args(std::get<2>(cmdl));

                // Execute the command
                ExecuteCommand(command, args);*/
                std::cout << "Executing commands with arguments is not supported yet in the Command Line Interface" << std::endl << std::endl;
            }
            else if(in.empty()); // Do nothing
            else {
                ExecuteCommand(in);
            }
        }
        
        return 0;
    }
    else // Process the command directly
    {
        // Parse the arguments
        Command::Args args;
        for(size_t i = 2; i < argc; i++)
        {
            args.Push(argv[i]);
        }

        // Execute the command
        return ExecuteCommand(argv[1], args);
    }
    
    return 0;
}
