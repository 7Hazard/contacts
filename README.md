# contacts

This is a simple app for managing contacts and was primarily made for a school project.

It's written in C++ and features a command line interface including direct commands via arguments.

## Building
### Linux
Requirements
* CMake
* A compiler (Clang preferably)

### Windows
* CMake
* Visual Studio 2017 Community

To build, run ``./build``. On Windows, if you're using cmd.exe, you might have to remove the ``./`` in the beginning.
